#pragma once
#include "TProcuraConstrutiva.h"

///////////////////////////////////////////////////////////////////////////////
//	TProcuraConstrutivaComCiclos class
///////////////////////////////////////////////////////////////////////////////
//	Author: Jos� Coelho
//	Last revision: 2007-08-31
//	Description: 
//    Superclasse de procuras no espa�o das solu��es parciais.
//    Esta classe tem uma �nica vari�vel, o estado de onde foi gerado,
//    deve criar as vari�veis necess�rias � implementa��o dos m�todos virtuais 
//    para o problema em concreto.
//    Esta classe assume que h� estados repetidos, utilizar TProcuraConstrutiva
//    caso tal n�o seja verdade.
///////////////////////////////////////////////////////////////////////////////
class TProcuraConstrutivaComCiclos :
	public TProcuraConstrutiva
{
public:
	TProcuraConstrutivaComCiclos(void);
	~TProcuraConstrutivaComCiclos(void);

	// estado que o gerou, ou NULL caso seja o estado inicial
	TProcuraConstrutivaComCiclos* pai;

	// Coloca em sucessores a lista de objectos sucessores (s�o alocados neste m�todo e t�m de ser apagados)
	// O custo n�o necessita de ser preenchido, caso seja sempre unit�rio
	// chamar o m�todo desta classe ap�s adicionar os sucessores para actualizar geracoes e expansoes
	// e actualizar pai de todos os sucessores
	// os sucessores que j� foram gerados desde o n� inicial s�o apagados
	virtual void Sucessores(TVector<TProcuraConstrutiva*>&sucessores, TVector<long>&custo);

	// verificar se � igual ao estado ou diferente
	bool operator!=(TProcuraConstrutivaComCiclos&estado) { return true; }
	virtual bool Distinto(TProcuraConstrutivaComCiclos*estado) { return true; }
};

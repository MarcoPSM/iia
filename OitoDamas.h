#pragma once
#include "TProcuraMelhorativa.h"

///////////////////////////////////////////////////////////////////////////////
//	COitoDamas class
///////////////////////////////////////////////////////////////////////////////
//	Author: Jos� Coelho
//	Last revision: 2007-09-06
//	Description: 
//    Implementa o problema das oito damas. Este problema consiste em colocar
//    oito damas de xadrez (movem-se na horizontal, vertical e diagonal), num
//    tabuleiro de xadrez, sem que estas se ataquem umas �s outras.
///////////////////////////////////////////////////////////////////////////////
class COitoDamas :
	public TProcuraMelhorativa
{
public:
	COitoDamas(void);
	~COitoDamas(void);

	// posi��o de cada dama
	TVector<int> damas; 

	// m�todos redefinidos de TProcuraConstrutiva

	// Cria um objecto que � uma c�pia deste
	TProcuraConstrutiva* Duplicar(void);
	void Copiar(TProcuraConstrutiva*objecto) {  
		TProcuraMelhorativa::Copiar(objecto);
		damas=((COitoDamas*)objecto)->damas; 
	}
	// Coloca o objecto no estado inicial da procura
	void SolucaoVazia(void) { damas.Count(0); }
	// Coloca em sucessores a lista de objectos sucessores (s�o alocados neste m�todo e t�m de ser apagados)
	// O custo n�o necessita de ser preenchido, caso seja sempre unit�rio
	void Sucessores(TVector<TProcuraConstrutiva*>&sucessores, TVector<long>&custo);
	// Retorna verdade caso o estado actual seja um estado objectivo
	bool SolucaoCompleta(void) { return damas.Count()==8; }	
	// Escrever informa��o de debug sobre o objecto currente 
	// (utilizar vari�vel TProcuraConstrutiva::debug para seleccionar o detalhe pretendido)
	void Debug(void);

	// m�todos redefinidos de TProcuraMelhorativa

	// � gerada uma nova solu��o aleatoriamente. 
	void NovaSolucao(void);
	// Retorna o valor da solu��o completa actual. 
	long Avaliar(void);
	// Operador de vizinhan�a (apenas em solu��es completas)
	// chamar este operador para actualiza��o de estat�sticas
	void Vizinhanca(TVector<TProcuraMelhorativa*>&vizinhos);
	// muta o estado actual (de acordo com a parametriza��o global)
	void Mutar(void);
	// cruza os dois elementos a e b, colocando o resultado neste
	void Cruzamento(TProcuraMelhorativa*a,TProcuraMelhorativa*b);
	// retorna a dist�ncia deste elemento ao elemento a 
	// (necess�rio para manter os elementos diversos)
	long Distancia(TProcuraMelhorativa*a);

};

#include "TProcuraAdversa.h"
#include <stdio.h>

// valor de infinito (vit�ria/derrota), omiss�o 1000
long TProcuraAdversa::infinito=1000;

TProcuraAdversa::TProcuraAdversa(void)
{
}

TProcuraAdversa::~TProcuraAdversa(void)
{
}

// retorna o valor do estado actual, ap�s procura de profundidade nivel
long TProcuraAdversa::MiniMax(long nivel)
{
	DebugChamada();
	if(nivel==0) {
		DebugCorte(-1,minimizar);
		int resultado=Heuristica();
		if(debug>1) printf(" %d",resultado);
		return resultado;
	} else {
		long resultado=0, melhor=-1;
		TVector<TProcuraConstrutiva*> sucessores;
		TVector<long> custo;
		Sucessores(sucessores,custo);
		if(sucessores.Count()==0) { // n� final
			DebugCorte(0,minimizar);
			int resultado=Heuristica();
			if(debug>1) printf(" %d",resultado);
			return resultado;
		}
		for(int i=0;i<sucessores.Count();i++) {
			DebugExpansao(i,sucessores.Count(),minimizar);
			long valor=((TProcuraAdversa*)sucessores[i])->MiniMax(nivel-1);
			if(i==0 || (minimizar?resultado>valor:resultado<valor)) {
				resultado=valor;
				melhor=i;
				if(nivel>1 && debug>=2) { // colocar valor actual alterado
					NovaLinha();
					printf("(%ld)",resultado);
				}
			}
		}
		if(melhor>=0) {
			if(solucao!=NULL)
				delete solucao;
			solucao=sucessores[melhor];
		}
		DebugCorte(sucessores.Count(),minimizar);
		LibertarVector(sucessores,melhor);
		return resultado;
	}	
}

// retorna o valor do estado actual, ap�s procura de profundidade nivel
long TProcuraAdversa::MiniMaxAlfaBeta(long nivel,long alfa,long beta)
{
	DebugChamada();
	if(nivel==0) {
		DebugCorte(-1,minimizar);
		int resultado=Heuristica();
		if(debug>1) printf(" %d",resultado);
		return resultado;
	} else {
		long resultado=0,melhor=-1;
		TVector<TProcuraConstrutiva*> sucessores;
		TVector<long> custo;
		Sucessores(sucessores,custo);
		if(sucessores.Count()==0) { // n� final
			DebugCorte(0);
			int resultado=Heuristica();
			if(debug>1) printf(" %d",resultado);
			return resultado;
		}
		for(int i=0;i<sucessores.Count();i++) {
			DebugExpansao(i,sucessores.Count(),minimizar);
			long valor=((TProcuraAdversa*)sucessores[i])->MiniMaxAlfaBeta(nivel-1,alfa,beta);
			if(i==0 || (minimizar?resultado>valor:resultado<valor)) {
				resultado=valor;
				melhor=i;
				if(nivel>1 && debug>=2) { // colocar valor actual alterado
					NovaLinha();
					printf("(%d)",resultado);
				}
			}
			// cortes e actualiza��o
			if(i<sucessores.Count()-1) { // n�o interessa cortar quando n�o h� mais nada para cortar
				if(minimizar) { // pretas
					if(alfa>=resultado) {
						if(debug>1) {
							// substituir o �ltimo caracter por um corte
							ramo.Last()=223;
							NovaLinha();
							printf(" alfa");
						}
						break; // as brancas t�m uma alternativa, � escusado continuar a procurar aqui
					}
					if(beta>resultado)
						beta=resultado;
				} else { // brancas
					if(beta<=resultado) {
						if(debug>1) {
							ramo.Last()=223;
							NovaLinha();
							printf(" beta");
						}
						break; // as pretas t�m uma alternativa, � escusado continuar a procurar aqui
					}
					if(alfa<resultado)
						alfa=resultado;
				}
			}
		}
		if(melhor>=0) {
			if(solucao!=NULL)
				delete solucao;
			solucao=sucessores[melhor];
		}
		DebugCorte(sucessores.Count(),minimizar);
		LibertarVector(sucessores,melhor);
		return resultado;
	}	
}

// M�todo para teste manual do objecto (chamadas aos algoritmos, constru��o de uma solu��o manual)
// Este m�todo destina-se a testes preliminares, e deve ser redefinido apenas se forem definidos novos algoritmos 
void TProcuraAdversa::TesteManual(const char *nome,long seed)
{
	char str[256];
	long limiteNivel=4;
	LimparEstatisticas();
	TRand::srand(seed);
	SolucaoVazia();
	while(true) {
		printf("\n\
%s (TProcuraAdversa)\n\
[Configuracoes] debug %d | limiteNivel %d\n\
[Estatisticas] expansoes %d | geracoes %d | avaliacoes %d ",
			nome,debug,limiteNivel,expansoes,geracoes,avaliacoes);
		Debug();
		printf("\n\
_______________________________________________________________________________\n\
1 - LimparEstatisticas | 2 - SolucaoVazia | 3 - Heuristica      [Inicializacao]\n\
4 - MiniMax | 5 - MiniMaxAlfaBeta                           [Procuras Adversas]\n\
6 - debug | 7 - limiteNivel                                     [Configuracoes]\n\
8 - Sucessores                                                         [Testes]\n\
Opcao:");
		gets(str);
		if(str[0]==0) return;
		switch(atoi(str)) {
			case 1: LimparEstatisticas(); break;
			case 2: TRand::srand(seed); SolucaoVazia(); break;
			case 3: printf("\nHeur�stica: %d\n",Heuristica()); break;
			case 4: 
				LimparEstatisticas(); 
				printf("\nResultado MiniMax(%d): %d",limiteNivel,MiniMax(limiteNivel)); 
				if(solucao!=NULL) {
					Copiar(solucao); 
					delete solucao; 
				}
				solucao=NULL; 
				break;
			case 5: 
				LimparEstatisticas(); 
				printf("\nResultado MiniMaxAlfaBeta(%d): %d",limiteNivel,MiniMaxAlfaBeta(limiteNivel));  
				if(solucao!=NULL) {
					Copiar(solucao); 
					delete solucao; 
				}
				solucao=NULL; 
				break;
			case 6: printf("\nNovo valor para debug: "); gets(str); debug=atoi(str); break;
			case 7: printf("\nNovo valor para limiteNivel: "); gets(str); limiteNivel=atoi(str); break;
			case 8: // sucessores 
				{
					TVector<TProcuraConstrutiva*> sucessores;
					TVector<long> custos;
					int opcao;
					do {
						Sucessores(sucessores,custos);
						printf("\nEstado actual:");
						Debug();
						printf("\nHeur�stica: %d\n",Heuristica());
						printf("\nSucessores: %d",sucessores.Count());
						printf("\nMenu:\n1 a %d - vizualizar sucessor\n-1 a -%d - mover para sucessor\n 0 - sair.\nOp��o:",
							sucessores.Count(),sucessores.Count());
						gets(str); 
						opcao=atoi(str);
						if(opcao>0 && opcao<=sucessores.Count()) {
							printf("\nSucessor %d",opcao);
							sucessores[opcao-1]->Debug();
							printf("\nHeur�stica: %d\n",sucessores[opcao-1]->Heuristica());
						} else if(opcao<0 && opcao>=-sucessores.Count()) {
							Copiar(sucessores[-opcao-1]);
						}
						LibertarVector(sucessores);
					} while(opcao!=0);
				}
				break;
			case 9: return;
			default: printf("\nOp��o n�o definida."); break;
		}
	}
}

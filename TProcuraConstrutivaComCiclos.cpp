#include "TProcuraConstrutivaComCiclos.h"

TProcuraConstrutivaComCiclos::TProcuraConstrutivaComCiclos(void) : pai(NULL)
{
}

TProcuraConstrutivaComCiclos::~TProcuraConstrutivaComCiclos(void)
{
}


// chamar o m�todo desta classe ap�s adicionar os sucessores para actualizar geracoes e expansoes
// e actualizar pai de todos os sucessores
void TProcuraConstrutivaComCiclos::Sucessores(TVector<TProcuraConstrutiva*>&sucessores, TVector<long>&custo)
{
	// remover todos os estados que j� tenham sido expandidos neste ramo
	for(int i=0;i<sucessores.Count();i++) {
		TProcuraConstrutivaComCiclos *avo=this;
		TProcuraConstrutivaComCiclos *sucessor=(TProcuraConstrutivaComCiclos *)sucessores[i];
		while(avo!=NULL && avo->Distinto(sucessor))//*avo!=*sucessor)
			avo=avo->pai;
		if(avo!=NULL) { 
			delete sucessor;
			sucessores[i]=NULL;
		}
	}
	sucessores.Remove(NULL);
	// actualizar os pais
	for(int i=0;i<sucessores.Count();i++)
		((TProcuraConstrutivaComCiclos*)sucessores[i])->pai=this;
	// actualizar estat�sticas
	TProcuraConstrutiva::Sucessores(sucessores,custo);
}
#pragma once

#include "TVector.h"

///////////////////////////////////////////////////////////////////////////////
//	TProcuraConstrutiva class
///////////////////////////////////////////////////////////////////////////////
//	Author: Jos� Coelho
//	Last revision: 2007-08-31
//	Description: 
//    Superclasse de procuras no espa�o das solu��es parciais (a solu��o � constru�da).
//    Esta classe n�o tem vari�veis, deve criar as vari�veis necess�rias 
//    � implementa��o dos m�todos virtuais para o problema em concreto.
//    Esta classe assume que n�o h� ciclos, utilizar TProcuraConstrutivaComCiclos
//    caso tal n�o seja verdade.
///////////////////////////////////////////////////////////////////////////////
class TProcuraConstrutiva
{
protected:
	// m�todo interno para libertar objectos n�o necess�rios
	void LibertarVector(TVector<TProcuraConstrutiva*>&vector, int excepto=-1);
public:
	// classe sem vari�veis, construtor e destrutores s�o vazios
	TProcuraConstrutiva(void) {}
	virtual ~TProcuraConstrutiva(void) {}


	///////////////////////////////////////////////////////////////////////////////
	// Vari�veis globais � classe. Estas vari�veis s�o reutilizadas para cada corrida.
	// O facto de serem globais evita que sejam copiadas vezes sem sentido, mas impede que 
	// se possa fazer mais que uma corrida em simult�neo.
	// Se necessitar de duas ou mais corridas em simult�neo, tem de remover estas vari�veis globais. 
	///////////////////////////////////////////////////////////////////////////////

	// n�vel de debug: 0 - nada; 1 - actividade; 2 - passos; 3 - detalhe
	static int debug; 
	// n�mero de gera��es de estados 
	static long geracoes;
	// n�mero de expans�es de estados
	static long expansoes; 
	// n�mero de chamadas � fun��o heur�stica
	static long avaliacoes; 
	// auxiliar para constru��o da �rvore de procura
	static TVector<unsigned char> ramo;
	// espa�amento entre ramos da �rvore de debug
	static int espacosRamo;
	// construir ou n�o o caminho do estado inicial at� � solu��o
	static bool calcularCaminho;
	// valor retornado pela procura (tem de ser libertado)
	static TVector<TProcuraConstrutiva*> caminho;
	// valor retornado pela procura (tem de ser libertado)
	static TProcuraConstrutiva* solucao;


	///////////////////////////////////////////////////////////////////////////////
	// M�todos para redefinir conforme o problema
	///////////////////////////////////////////////////////////////////////////////

	// Cria um objecto que � uma c�pia deste
	virtual TProcuraConstrutiva* Duplicar(void) = 0;
	// Fica com uma c�pia de objecto
	virtual void Copiar(TProcuraConstrutiva*objecto) { }
	// Coloca o objecto no estado inicial da procura
	virtual void SolucaoVazia(void) {}
	// Coloca em sucessores a lista de objectos sucessores (s�o alocados neste m�todo e t�m de ser apagados)
	// O custo n�o necessita de ser preenchido, caso seja sempre unit�rio
	// chamar o m�todo desta classe ap�s adicionar os sucessores para actualizar geracoes e expansoes
	virtual void Sucessores(TVector<TProcuraConstrutiva*>&sucessores, TVector<long>&custo) {
		expansoes++;
		geracoes+=sucessores.Count();	
	}
	// Retorna verdade caso o estado actual seja um estado objectivo
	virtual bool SolucaoCompleta(void) { return false; }
	// Escrever informa��o de debug sobre o objecto currente 
	// (utilizar vari�vel TProcuraConstrutiva::debug para seleccionar o detalhe pretendido)
	virtual void Debug(void);

	// Redefinir para poder utilizar os algoritmos informados
	// O custo desde o n� inicial � tido em conta, esta fun��o deve devolver o custo estimado
	// nunca sobre estimando, deste estado at� ao n� final mais pr�ximo (� um m�nimo)
	// chamar para actualia��o de avaliacoes
	virtual long Heuristica(void) { avaliacoes++; return 0; }


	///////////////////////////////////////////////////////////////////////////////
	// Algoritmos de procura cega (chamar ap�s colocar o estado inicial no objecto)
	// N�o necessitam da implementa��o da fun��o heur�stica
	///////////////////////////////////////////////////////////////////////////////

	// retorna o valor da solu��o e coloca o caminho no vector (se calcularCaminho=true), ou -1 caso n�o encontre solu��o
	// limite � o n�mero de estados gerados n�o expandidos, que n�o pode ultrapassar esse limite 
	// os que ultrapassarem s�o deitados fora (se 0 este limite n�o importa, podendo haver problemas de mem�ria)
	long LarguraPrimeiro(long limite=0);

	// retorna o valor da solu��o e coloca o caminho no vector, ou -1 caso n�o encontre solu��o
	long CustoUniforme(long limite=0);

	// retorna o valor da solu��o e coloca o caminho no vector, ou -1 caso n�o encontre solu��o
	// caso o nivel=-1, � feita uma procura em profunidade normal
	// caso o nivel>0, � feita uma procura em profundidade limitada
	// caso o n�vel=0, � feita uma procura em profundidade iterativa, sem limite;
	long ProfundidadePrimeiro(long nivel=-1);


	///////////////////////////////////////////////////////////////////////////////
	// Algoritmos de procura informados (chamar ap�s colocar o estado inicial no objecto)
	// Necessitam da implementa��o da fun��o Heur�stica
	///////////////////////////////////////////////////////////////////////////////

	// id�ntico ao algoritmo em profundidade, mas os sucessores s�o expandidos por ordem da fun��o heur�stica
	long MelhorPrimeiro(long nivel=-1);

	// id�ntico ao algoritmo Custo Uniforme, mas o custo � contabilizado tamb�m com a fun��o heur�stica
	// retorna o valor da solu��o e coloca o caminho no vector, ou -1 caso n�o encontre solu��o
	long AStar(long limite=0);

protected:
	// M�todo para ser chamado antes de analisar cada sucessor
	void DebugExpansao(int sucessor, int sucessores,bool duplo=false);
	// M�todo para ser chamado quando n�o h� sucessores ou h� um corte de profundidade
	void DebugCorte(int sucessores=-1,bool duplo=false);
	// Encontrou uma solu��o
	void DebugSolucao(void);
	// Informa��o de debug na chamada ao m�todo recursivo
	void DebugChamada(void);
	// Chamar sempre que se quer uma nova linha com a �rvore em baixo
	void NovaLinha(bool tudo=true);
	// Passo no algoritmo em largura
	void DebugPasso(int custo=0, int heuristica=0);
public:
	// Chamar antes de iniciar uma procura
	void LimparEstatisticas(void);
	// M�todo para teste manual do objecto (chamadas aos algoritmos, constru��o de uma solu��o manual)
	// Este m�todo destina-se a testes preliminares, e deve ser redefinido apenas se forem definidos novos algoritmos 
	virtual void TesteManual(const char *nome, long seed=1);
};

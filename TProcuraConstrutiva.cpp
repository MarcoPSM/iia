#include "TProcuraConstrutiva.h"
#include <stdio.h>

// n�vel de debug: 0 - nada; 1 - actividade; 2 - passos; 3 - detalhe
int TProcuraConstrutiva::debug=0;
// n�mero de gera��es de estados
long TProcuraConstrutiva::geracoes=0;
// n�mero de expans�es de estados
long TProcuraConstrutiva::expansoes=0; 
// n�mero de chamadas � fun��o heur�stica
long TProcuraConstrutiva::avaliacoes=0; 
// auxiliar para constru��o da �rvore de procura
TVector<unsigned char> TProcuraConstrutiva::ramo;
// espa�amento entre ramos da �rvore de debug
int TProcuraConstrutiva::espacosRamo=1;
// construir ou n�o o caminho do estado inicial at� � solu��o
bool TProcuraConstrutiva::calcularCaminho=false;
// valor retornado pela procura (tem de ser libertado)
TVector<TProcuraConstrutiva*> TProcuraConstrutiva::caminho;
// valor retornado pela procura (tem de ser libertado)
TProcuraConstrutiva* TProcuraConstrutiva::solucao=NULL;


// m�todo interno para libertar objectos n�o necess�rios
void TProcuraConstrutiva::LibertarVector(TVector<TProcuraConstrutiva*>&vector, int excepto)
{
	for(int i=0;i<vector.Count();i++)
		if(i!=excepto && vector[i]!=NULL)
			delete vector[i];
	vector.Count(0);
}


// retorna o valor da solu��o e coloca o caminho no vector, ou -1 caso n�o encontre solu��o
long TProcuraConstrutiva::LarguraPrimeiro(long limite)
{
	TVector<TProcuraConstrutiva*> lista;
	TVector<long> pais,custoTotal;
	lista.Add(this);
	pais.Add(-1);
	custoTotal.Add(0);
	for(int i=0;i<lista.Count();i++) {
		lista[i]->DebugPasso(custoTotal[i]);
		if(lista[i]->SolucaoCompleta()) { // teste da fun��o Objectivo
			if(calcularCaminho) { // retornar todo o caminho
				caminho.Add(lista[i]);
				// obter caminho para a solu��o
				for(int j=pais[i];j>0;j=pais[j]) {
					caminho.Add(lista[j]);
					if(debug>1) printf(" (%d) ",custoTotal[j]);
					if(debug>0) lista[j]->Debug();
					lista[j]=NULL;
				}
			} 
			solucao=lista[i];
			lista[i]->DebugSolucao();
			lista[i]=NULL;
			LibertarVector(lista,0);
			return custoTotal[i];
		} else { // expans�o do n�
			TVector<TProcuraConstrutiva*>sucessores;
			TVector<long> custo;
			lista[i]->Sucessores(sucessores,custo); 
			if(limite) {
				// remover elementos a mais da lista de sucessores
				if(limite<lista.Count()-i+sucessores.Count()) {
					long maximo=limite-lista.Count()+i;
					if(maximo>=0 && maximo<sucessores.Count()) {
						for(int i=maximo;i<sucessores.Count();i++)
							delete sucessores[i];
						sucessores.Count(maximo);
					} else if(maximo<0) {
						LibertarVector(sucessores);
					}
				}
			}
			for(int j=0;j<sucessores.Count();j++) {
				pais.Add(i);
				custoTotal.Add(custoTotal[i]+(custo.Count()>0?custo[j]:1));
			}
			lista+=sucessores; 
			// N�o se pode libertar estados j� expandidos porque n�o se sabe se 
			// os pais s�o necess�rios ou n�o. 
		}
	}
	LibertarVector(lista,0);
	return -1; // falhou
}


// retorna o valor da solu��o e coloca o caminho no vector, ou -1 caso n�o encontre solu��o
// 090318: bug ao remover elementos da lista
long TProcuraConstrutiva::CustoUniforme(long limite)
{
	TVector<TProcuraConstrutiva*> lista;
	TVector<long> pais,custoTotal;
	TVector<long> id;
	lista.Add(this);
	pais.Add(-1);
	custoTotal.Add(0);
	id.Add(0); // primeiro elemento � o 0
	for(int i=0;i<id.Count();i++) {
		lista[id[i]]->DebugPasso(custoTotal[id[i]]);
		if(lista[id[i]]->SolucaoCompleta()) { // teste da fun��o Objectivo
			if(calcularCaminho) { // retornar todo o caminho
				caminho.Add(lista[id[i]]);
				// obter caminho para a solu��o
				for(int j=pais[id[i]];j>0;j=pais[j]) {
					caminho.Add(lista[j]);
					if(debug>1) printf(" (%d) ",custoTotal[j]);
					if(debug>0) lista[j]->Debug();
					lista[j]=NULL;
				}
			} 
			solucao=lista[id[i]];
			lista[id[i]]->DebugSolucao();
			lista[id[i]]=NULL;
			LibertarVector(lista,0);
			return custoTotal[id[i]];
		} else { // expans�o do n�
			TVector<TProcuraConstrutiva*>sucessores;
			TVector<long> custo;
			lista[id[i]]->Sucessores(sucessores,custo); 
			for(int j=0;j<sucessores.Count();j++) {
				pais.Add(id[i]);
				custoTotal.Add(custoTotal[id[i]]+(custo.Count()>0?custo[j]:1));
				// procurar o elemento com maior valor para inserir este elemento antes
				int l=i+1;
				while(l<id.Count() && custoTotal[id[l]]<=custoTotal.Last())
					l++;
				if(l<id.Count())
					id.Insert(custoTotal.Count()-1,l);
				else id.Add(custoTotal.Count()-1);
			}
			lista+=sucessores; // n�o pode apagar nada porque podem ser pais, sempre a somar no fim da lista
			if(limite) {
				// remover elementos a mais da lista de sucessores, com a estimativa mais elevada
				while(lista.Count()-i>limite) {
					// como o �ltimo elemento est� ap�s i, n�o foi expandido, pelo que n�o tem filhos
					long idRemover=id.Pop();
					delete lista[idRemover];
					lista.Delete(idRemover);
					custoTotal.Delete(idRemover);
					pais.Delete(idRemover);
					// ids com valor superior a idRemover, devem ser decrementados
					for(int j=0;j<id.Count();j++)
						if(id[j]>idRemover)
							id[j]--;
				}
			}
		}
	}
	LibertarVector(lista,0);
	return -1; // falhou
}

// retorna o valor da solu��o e coloca o caminho no vector, ou -1 caso n�o encontre solu��o
// caso o nivel=-1, � feita uma procura em profunidade normal
// caso o nivel>0, � feita uma procura em profundidade limitada
// caso o n�vel=0, � feita uma procura em profundidade iterativa, sem limite;
long TProcuraConstrutiva::ProfundidadePrimeiro(long nivel)
{
	DebugChamada();
	if(nivel==0) { // m�todo iterativo
		if(debug>=2) printf("\nN�vel 1\n");
		int resultado=ProfundidadePrimeiro(++nivel);
		while(resultado==-1) {
			if(debug>=2) printf("\nN�vel %d\n",nivel+1);
			resultado=ProfundidadePrimeiro(++nivel);
		}
		return resultado;
	} else { // m�todo normal
		if(SolucaoCompleta()) { // um n� objectivo!
			DebugSolucao();
			solucao=this;
			return 0;
		} 
		if(nivel>1 || nivel<0) { // continuar a procura
			// ainda n�o � o n� objectivo
			TVector<TProcuraConstrutiva*> sucessores;
			TVector<long> custo;
			Sucessores(sucessores,custo);
			// tentar todos os sucessores, um de cada vez
			for(int i=0;i<sucessores.Count();i++) {
				DebugExpansao(i,sucessores.Count());
				int resultado=sucessores[i]->ProfundidadePrimeiro(nivel-1);
				if(resultado>=0) { // este sucessor resolveu o problema, devolver
					LibertarVector(sucessores,i);
					if(calcularCaminho)
						caminho.Add(sucessores[i]); // adicionar este n� ao caminho
					if(custo.Count()>i) // caso exista custo, adicion�-lo
						resultado+=custo[i];
					else resultado++;
					if(debug>1) 
						printf(" (%d) ",resultado);
					DebugSolucao();
					return resultado;
				}
			}
			DebugCorte(sucessores.Count());
			LibertarVector(sucessores);
		} else DebugCorte();
		// percorreram-se todos os sucessores e nada (ou atingiu-se o limite), devolver -1
		return -1; 
	}
}

// id�ntico ao algoritmo em profundidade, mas os sucessores s�o expandidos por ordem da fun��o heur�stica
// n�o � necess�rio implementar custo nos sucessores para este algoritmo, dado que as decis�es s�o sempre locais
// retorna o valor da solu��o e coloca o caminho no vector, ou -1 caso n�o encontre solu��o
long TProcuraConstrutiva::MelhorPrimeiro(long nivel)
{
	DebugChamada();
	
	if(SolucaoCompleta()) { // um n� objectivo!
		DebugSolucao();
		solucao=this;
		return 0;
	} 
	if(nivel<0 || nivel>1) { // continuar a procura
		// ainda n�o � o n� objectivo
		TVector<TProcuraConstrutiva*> sucessores;
		TVector<long> custo;
		TVector<long> id;
		Sucessores(sucessores,custo);
		// calcular as heur�sticas
		{
			TVector<int> heuristicas;
			for(int i=0;i<sucessores.Count();i++) {
				heuristicas.Add(sucessores[i]->Heuristica());
/*				if(livre>=0 && custo[i]+heuristicas.Last()>=livre) { // n�o melhorar� o melhor actual
					heuristicas.Last()=-1;
					delete sucessores[i];
					sucessores[i]=NULL;
					custo[i]=-1;
				}*/
			}
			heuristicas.Remove(-1);
			sucessores.Remove(NULL);
			custo.Remove(-1);
			heuristicas.Sort(&id);
		}
		// tentar todo os sucessores, um de cada vez
		for(int i=0;i<sucessores.Count();i++) {
			DebugExpansao(i,sucessores.Count());
			int resultado=sucessores[id[i]]->MelhorPrimeiro(nivel-1);
			if(resultado>=0) { // este sucessor resolveu o problema, devolver
				LibertarVector(sucessores,id[i]);
				if(calcularCaminho)
					caminho.Add(sucessores[id[i]]); // adicionar este n� ao caminho
				if(custo.Count()>id[i]) // caso exista custo, adicion�-lo
					resultado+=custo[id[i]];
				else resultado++;
				if(debug>1) 
					printf(" (%d) ",resultado);
				DebugSolucao();
				return resultado;
			}
		}
		DebugCorte(sucessores.Count());
		LibertarVector(sucessores);
	} else DebugCorte();
	// percorreram-se todos os sucessores e nada (ou atingiu-se o limite), devolver -1
	return -1; 
}

// id�ntico ao algoritmo Custo Uniforme, mas o custo � contabilizado tamb�m com a fun��o heur�stica
// retorna o valor da solu��o e coloca o caminho no vector, ou -1 caso n�o encontre solu��o
// retorna o valor da solu��o e coloca o caminho no vector, ou -1 caso n�o encontre solu��o
// 090318: bug ao remover elementos da lista
long TProcuraConstrutiva::AStar(long limite)
{
	TVector<TProcuraConstrutiva*> lista;
	TVector<long> pais,custoTotal,estimativaTotal;
	TVector<long> id;
	lista.Add(this);
	pais.Add(-1);
	custoTotal.Add(0);
	estimativaTotal.Add(0);
	id.Add(0); // primeiro elemento � o 0
	for(int i=0;i<id.Count();i++) {
		lista[id[i]]->DebugPasso(custoTotal[id[i]],estimativaTotal[id[i]]);
		if(lista[id[i]]->SolucaoCompleta()) { // teste da fun��o Objectivo
			if(calcularCaminho) {
				caminho.Add(lista[id[i]]);
				// obter caminho para a solu��o
				for(int j=pais[id[i]];j>0;j=pais[j]) {
					caminho.Add(lista[j]);
					if(debug>1) printf(" (%d) ",custoTotal[j]);
					if(debug>0) lista[j]->Debug();
					lista[j]=NULL;
				}
			}
			solucao=lista[id[i]];
			lista[id[i]]->DebugSolucao();
			lista[id[i]]=NULL;
			LibertarVector(lista,0);
			return custoTotal[id[i]];
		} else { // expans�o do n�
			TVector<TProcuraConstrutiva*>sucessores;
			TVector<long> custo;
			lista[id[i]]->Sucessores(sucessores,custo); 
			for(int j=0;j<sucessores.Count();j++) {
				pais.Add(id[i]);
				custoTotal.Add(custoTotal[id[i]]+(custo.Count()>0?custo[j]:1));
				estimativaTotal.Add(custoTotal.Last()+sucessores[j]->Heuristica());
				// procurar o elemento com maior valor para inserir este elemento antes
				int l=i+1;
				while(l<id.Count() && estimativaTotal[id[l]]<=estimativaTotal.Last())
					l++;
				if(l<id.Count())
					id.Insert(estimativaTotal.Count()-1,l);
				else id.Add(estimativaTotal.Count()-1);
			}
			lista+=sucessores; 
			if(limite) {
				// remover elementos a mais da lista de sucessores, com a estimativa mais elevada
				while(lista.Count()-i>limite) {
					// como o �ltimo elemento est� ap�s i, n�o foi expandido, pelo que n�o tem filhos
					long idRemover=id.Pop();
					delete lista[idRemover];
					lista.Delete(idRemover);
					custoTotal.Delete(idRemover);
					estimativaTotal.Delete(idRemover);
					pais.Delete(idRemover);
					// ids com valor superior a idRemover, devem ser decrementados
					for(int j=0;j<id.Count();j++)
						if(id[j]>idRemover)
							id[j]--;
				}
			}
		}
	}
	LibertarVector(lista,0);
	return -1; // falhou
}



// Escrever informa��o de debug sobre o objecto currente (utilizar vari�vel TProcuraConstrutiva::debug para seleccionar o detalhe pretendido)
void TProcuraConstrutiva::Debug(void)
{
	if(debug>0)
		printf("\nTProcuras::Debug n�o definido, n�o � assim poss�vel mostrar informa��o relativa a este estado.");
}

// M�todo para ser chamado antes de analisar cada sucessor
void TProcuraConstrutiva::DebugExpansao(int sucessor, int sucessores,bool duplo)
{
	if(debug>=2) {
		if(sucessor>0) {
			NovaLinha(false);
		}
		if(sucessor==0 && sucessores==1) { // s� um ramo
			printf("%*c%c",espacosRamo,196,(duplo?186:197));//215:197));
			ramo.Add(32); // a ser impresso nesta posi��o nas linhas seguintes
		} else if(sucessor==0) { // in�cio e continua
			printf("%*c%c",espacosRamo,196,(duplo?203:194));//210:194));
			ramo.Add(duplo?186:179); // a ser impresso nesta posi��o nas linhas seguintes
		} else if(sucessor>0 && sucessor<sucessores-1) { // no meio e continua
			printf("%*s%c",espacosRamo," ",(duplo?186:195));//199:195));
			ramo.Last()=(duplo?186:179); 
		} else {
			printf("%*s%c",espacosRamo," ",(duplo?200:192));//211:192)); // no fim, vai acabar
			ramo.Last()=32; // a ser impresso nesta posi��o nas linhas seguintes
		}
	}
}

// M�todo para ser chamado quando n�o h� sucessores ou h� um corte de profundidade
void TProcuraConstrutiva::DebugCorte(int sucessores,bool duplo)
{
	if(debug>=2) {
		if(sucessores<0) {
			printf("%c %d/%d",221,expansoes,geracoes); // corte de profundidade
			if(avaliacoes) printf("/%d",avaliacoes);
			if(debug>=3) 
				Debug();
		} else if(sucessores>0) 
			ramo.Pop();
		else { // ramo em que n�o � poss�vel continuar
			printf("%c %d/%d",177,expansoes,geracoes);
			if(avaliacoes) printf("/%d",avaliacoes);
			if(debug>=3) 
				Debug();
		}
	}
}

// Encontrou uma solu��o
void TProcuraConstrutiva::DebugSolucao(void)
{
	if(debug>0 && SolucaoCompleta()) {
		printf(" Solucao encontrada!");
		ramo.Count(0);
		Debug();
	} else {
		if(debug>1) Debug();
		if(debug>=2) ramo.Pop();
	}
}

// Informa��o de debug na chamada ao m�todo recursivo
void TProcuraConstrutiva::DebugChamada(void)
{
	if(debug==1 && expansoes%1000==0) 
		printf("#");
//	else if(debug==2) printf("%c",196);
//	else if(debug>2) Debug();
}

// Chamar sempre que se quer uma nova linha com a �rvore em baixo
void TProcuraConstrutiva::NovaLinha(bool tudo)
{
	printf("\n");
	for(int i=0;i<ramo.Count()-(tudo?0:1);i++) 
		printf("%*s%c",espacosRamo," ",ramo[i]);
}

// Passo no algoritmo em largura
void TProcuraConstrutiva::DebugPasso(int custo, int heuristica)
{
	if(debug==1 && expansoes%1000==0) 
		printf("#");
	if(debug>=2) {
		printf("\n%02d: %d/%d",custo,expansoes,geracoes);
		if(avaliacoes) printf("/%d",avaliacoes);
		if(heuristica) printf("(%d)",heuristica);
	}
	if(debug>2)
		Debug();
}

// Chamar antes de iniciar uma procura
void TProcuraConstrutiva::LimparEstatisticas(void)
{
	expansoes=geracoes=avaliacoes=0;
	ramo.Count(0);
	if(calcularCaminho)
		LibertarVector(caminho,0);
	if(solucao!=NULL)
		delete solucao;
	solucao=NULL;
}

// M�todo para teste manual do objecto (chamadas aos algoritmos, constru��o de uma solu��o manual)
// Este m�todo destina-se a testes preliminares, e deve ser redefinido apenas se forem definidos novos algoritmos 
void TProcuraConstrutiva::TesteManual(const char *nome, long seed)
{
	char str[256];
	long limiteNivel=10;
	LimparEstatisticas();
	TRand::srand(seed); 
	SolucaoVazia();
	while(true) {
		printf("\n\
%s (TProcuraConstrutiva)\n\
[Configuracoes] debug %d | calcularCaminho %d | limiteNivel %d\n\
[Estatisticas] expansoes %d | geracoes %d | avaliacoes %d ",
			nome,debug,calcularCaminho?1:0,limiteNivel,expansoes,geracoes,avaliacoes);
		Debug();
		printf("\n\
_______________________________________________________________________________\n\
1 - LimparEstatisticas | 2 - SolucaoVazia | 3 - Heuristica      [Inicializacao]\n\
4 - LarguraPrimeiro | 5 - CustoUniforme | 6 - Prof.Primeiro    [Procuras Cegas]\n\
7 - MelhorPrimeiro | 8 - AStar                            [Procuras Informadas]\n\
9 - debug | 10 - calcularCaminho | 11 - limiteNivel             [Configuracoes]\n\
12 - Sucessores                                                        [Testes]\n\
Opcao:");
		gets(str);
		if(str[0]==0) return;
		switch(atoi(str)) {
			case 1: LimparEstatisticas(); break;
			case 2: TRand::srand(seed); SolucaoVazia(); break;
			case 3: printf("\nHeuristica: %d\n",Heuristica()); break;
			case 4: 
				LimparEstatisticas(); 
				printf("\nResultado LarguraPrimeiro(%d): %d",limiteNivel,LarguraPrimeiro(limiteNivel)); 
				if(solucao!=NULL) {
					Copiar(solucao); 
					delete solucao; 
					solucao=NULL; 
				}
				break;
			case 5: 
				LimparEstatisticas(); 
				printf("\nResultado CustoUniforme(%d): %d",limiteNivel,CustoUniforme(limiteNivel));  
				if(solucao!=NULL) {
					Copiar(solucao); 
					delete solucao; 
					solucao=NULL; 
				}
				break;
			case 6: 
				LimparEstatisticas(); 
				printf("\nResultado ProfundidadePrimeiro(%d): %d",limiteNivel,ProfundidadePrimeiro(limiteNivel));  
				if(solucao!=NULL) {
					Copiar(solucao); 
					delete solucao; 
					solucao=NULL; 
				}
				break;
			case 7: 
				LimparEstatisticas(); 
				printf("\nResultado MelhorPrimeiro(%d): %d",limiteNivel,MelhorPrimeiro(limiteNivel));  
				if(solucao!=NULL) {
					Copiar(solucao); 
					delete solucao; 
					solucao=NULL; 
				}
				break;
			case 8: 
				LimparEstatisticas(); 
				printf("\nResultado AStar(%d): %d",limiteNivel,AStar(limiteNivel));  
				if(solucao!=NULL) {
					Copiar(solucao); 
					delete solucao; 
					solucao=NULL; 
				}
				break;
			case 9: printf("\nNovo valor para debug: "); gets(str); debug=atoi(str); break;
			case 10: printf("\nNovo valor para cacularCaminho: "); gets(str); calcularCaminho=(atoi(str)!=0); break;
			case 11: printf("\nNovo valor para limiteNivel: "); gets(str); limiteNivel=atoi(str); break;
			case 12: // sucessores 
				{
					TVector<TProcuraConstrutiva*> sucessores;
					TVector<long> custos;
					int opcao;
					do {
						Sucessores(sucessores,custos);
						printf("\nEstado actual:");
						Debug();
						printf("\nHeuristica: %d\n",Heuristica());
						printf("\nSucessores: %d",sucessores.Count());
						printf("\nMenu:\n1 a %d - vizualizar sucessor\n-1 a -%d - mover para sucessor\n 0 - sair.\nOpcao:",
							sucessores.Count(),sucessores.Count());
						gets(str); 
						opcao=atoi(str);
						if(opcao>0 && opcao<=sucessores.Count()) {
							printf("\nSucessor %d",opcao);
							sucessores[opcao-1]->Debug();
							printf("\nHeuristica: %d\n",sucessores[opcao-1]->Heuristica());
						} else if(opcao<0 && opcao>=-sucessores.Count()) {
							Copiar(sucessores[-opcao-1]);
						}
						LibertarVector(sucessores);
					} while(opcao!=0);
				}
				break;
			case 13: return;
			default: printf("\nOpcao nao definida."); break;
		}
	}
}

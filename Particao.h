#pragma once

#include "TProcuraConstrutiva.h"

///////////////////////////////////////////////////////////////////////////////
//	CParticao class
///////////////////////////////////////////////////////////////////////////////
//	Author: Jos� Coelho
//	Last revision: 2007-09-06
//	Description: 
//    Implementa o problema da parti��o. Este problema consiste em dividir
//    um conjunto de n�meros naturais em duas partes cuja soma dos n�meros em cada
//    parte seja igual. 
///////////////////////////////////////////////////////////////////////////////
class CParticao :
	public TProcuraConstrutiva
{
public:
	CParticao(void);
	~CParticao(void);

	TVector<long> numeros, esquerda, direita;
	long totalEsquerda, totalDireita;

	// M�todos redefinidos de TProcuraConstrutiva

	// Cria um objecto que � uma c�pia deste
	TProcuraConstrutiva* Duplicar(void);
	void Copiar(TProcuraConstrutiva*objecto) {  
		numeros=((CParticao*)objecto)->numeros; 
		esquerda=((CParticao*)objecto)->esquerda; 
		direita=((CParticao*)objecto)->direita; 
		totalEsquerda=((CParticao*)objecto)->totalEsquerda; 
		totalDireita=((CParticao*)objecto)->totalDireita; 
	}
	// Coloca o objecto no estado inicial da procura
	void SolucaoVazia(void);
	// Coloca em sucessores a lista de objectos sucessores (s�o alocados neste m�todo e t�m de ser apagados)
	// O custo n�o necessita de ser preenchido, caso seja sempre unit�rio
	// chamar o m�todo desta classe ap�s adicionar os sucessores para actualizar geracoes e expansoes
	void Sucessores(TVector<TProcuraConstrutiva*>&sucessores, TVector<long>&custo);
	// Retorna verdade caso o estado actual seja um estado objectivo
	bool SolucaoCompleta(void) { return numeros.Count()==0 && totalEsquerda==totalDireita; }
	// Escrever informa��o de debug sobre o objecto currente 
	// (utilizar vari�vel TProcuraConstrutiva::debug para seleccionar o detalhe pretendido)
	void Debug(void);

};

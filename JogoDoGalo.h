#pragma once
#include "TProcuraAdversa.h"

///////////////////////////////////////////////////////////////////////////////
//	CJogoDoGalo class
///////////////////////////////////////////////////////////////////////////////
//	Author: Jos� Coelho
//	Last revision: 2007-09-06
//	Description: 
//    Implementa o Jogo do Galo. Este jogo � jogado num tabuleiro 3x3 por dois jogadores
//    em que cada jogador joga � vez numa posi��o vazia, colocando a sua marca.
//    Ganha o jogador que conseguir fazer 3 em linha com a sua marca.
///////////////////////////////////////////////////////////////////////////////
class CJogoDoGalo :
	public TProcuraAdversa
{
public:
	TVector<char> tabuleiro;
public:
	CJogoDoGalo(void);
	~CJogoDoGalo(void);

	// Cria um objecto que � uma c�pia deste
	TProcuraConstrutiva* Duplicar(void);
	// Fica com uma c�pia de objecto
	void Copiar(TProcuraConstrutiva*objecto);
	// Coloca o objecto no estado inicial da procura
	void SolucaoVazia(void);
	// Coloca em sucessores a lista de objectos sucessores (s�o alocados neste m�todo e t�m de ser apagados)
	// O custo n�o necessita de ser preenchido, caso seja sempre unit�rio
	// chamar o m�todo desta classe ap�s adicionar os sucessores para actualizar geracoes e expansoes
	void Sucessores(TVector<TProcuraConstrutiva*>&sucessores, TVector<long>&custo);
	// Retorna verdade caso o estado actual seja um estado objectivo
	bool SolucaoCompleta(void);
	// Escrever informa��o de debug sobre o objecto currente 
	// (utilizar vari�vel TProcuraConstrutiva::debug para seleccionar o detalhe pretendido)
	void Debug(void);

	// Redefinir para poder utilizar os algoritmos informados
	// O custo desde o n� inicial � tido em conta, esta fun��o deve devolver o custo estimado
	// nunca sobre estimando, deste estado at� ao n� final mais pr�ximo (� um m�nimo)
	// chamar para actualia��o de avaliacoes
	long Heuristica(void);
};

#pragma once
#include "TProcuraConstrutiva.h"

///////////////////////////////////////////////////////////////////////////////
//	TProcuraAdversa class
///////////////////////////////////////////////////////////////////////////////
//	Author: Jos� Coelho
//	Last revision: 2007-09-04
//	Description: 
//    Superclasse de procuras no espa�o das solu��es parciais (a solu��o � constru�da).
//    A procura adversa s� pode ser feita no espa�o das solu��es parciais, e � subclasse
//    da TProcuraConstrutiva. Os custos devem ser ignorados, apenas o valor da fun��o
//	  Heuristica � utilizado. O valor infinito � definido numa vari�vel global � 
//    classe e deve ser alterado conforme o problema, significando vit�ria/derrota.
//    Se n�o houver sucessores, o estado � terminal e o retorno da fun��o heur�stica
//    deve ser exacto: -infinito ganham pretas, 0 empate, +infinito ganham as brancas
///////////////////////////////////////////////////////////////////////////////
class TProcuraAdversa :
	public TProcuraConstrutiva
{
public:
	// o jogador actual deve minimizar o custo (ou maximizar caso tenha o valor falso)
	bool minimizar;
	// valor de infinito (vit�ria/derrota), omiss�o 1000
	static long infinito;
public:
	TProcuraAdversa(void);
	~TProcuraAdversa(void);

	///////////////////////////////////////////////////////////////////////////////
	// Algoritmo MiniMax
	///////////////////////////////////////////////////////////////////////////////

	// retorna o valor do estado actual, ap�s procura de profundidade nivel
	long MiniMax(long nivel=4);

	// retorna o valor do estado actual, ap�s procura de profundidade nivel
	long MiniMaxAlfaBeta(long nivel=4,long alfa=-infinito,long beta=+infinito);

	// M�todo para teste manual do objecto (chamadas aos algoritmos, constru��o de uma solu��o manual)
	// Este m�todo destina-se a testes preliminares, e deve ser redefinido apenas se forem definidos novos algoritmos 
	void TesteManual(const char *nome, long seed=1);
};

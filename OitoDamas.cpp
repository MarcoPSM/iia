#include "OitoDamas.h"
#include <stdio.h>

COitoDamas::COitoDamas(void) 
{
}

COitoDamas::~COitoDamas(void)
{
}

// Cria um objecto que � uma c�pia deste
TProcuraConstrutiva* COitoDamas::Duplicar(void)
{
	COitoDamas *clone=new COitoDamas;
	clone->damas=damas;
	return clone;
}

// Coloca em sucessores a lista de objectos sucessores (s�o alocados neste m�todo e t�m de ser apagados)
// O custo n�o necessita de ser preenchido, caso seja sempre unit�rio
void COitoDamas::Sucessores(TVector<TProcuraConstrutiva*>&sucessores, TVector<long>&custo)
{
	int novaLinha=damas.Count();
	// tentar colocar damas em todas as colunas
	for(int i=0;i<8;i++) {
		int j=0;
		// verificar se h� uma dama a atacar esta posi��o
		for(;j<novaLinha;j++)
			if(i==damas[j] || i==damas[j]+(novaLinha-j) || i==damas[j]-(novaLinha-j))
				break;
		if(j==novaLinha) { // � poss�vel, adicionar sucessor
			COitoDamas *sucessor=(COitoDamas*)Duplicar();
			sucessor->damas.Add(i);
			sucessores.Add(sucessor);
		}
	}
	TProcuraConstrutiva::Sucessores(sucessores,custo);
}

// Escrever informa��o de debug sobre o objecto currente 
// (utilizar vari�vel TProcuraConstrutiva::debug para seleccionar o detalhe pretendido)
void COitoDamas::Debug(void)
{
	for(int i=0;i<8;i++) {
		NovaLinha();
		for(int j=0;j<8;j++) {
			int cor=((i+j)%2?176:178);
			if(damas.Count()>i && damas[i]==j)
				printf("%c%c",16,17);
			else printf("%c%c",cor,cor);
		}
	}
	NovaLinha();
	if(damas.Count()==8 && custo>=0) {
		printf("Custo: %d",custo);
		NovaLinha();
	}
}

// � gerada uma nova solu��o aleatoriamente. 
void COitoDamas::NovaSolucao(void)
{
	damas.Count(8);
	for(int i=0;i<8;i++)
		damas[i]=TRand::rand()%8;
	custo=-1;
}

// Retorna o valor da solu��o completa actual. 
long COitoDamas::Avaliar(void)
{
	TProcuraMelhorativa::Avaliar();
	custo=0;
	// calcular o n�mero de pares de damas atacadas
	for(int i=0;i<7;i++) 
		for(int j=i+1;j<8;j++)
			if(damas[i]==damas[j] || damas[i]-damas[j]==i-j || damas[i]-damas[j]==j-i)
				custo++;
	return custo;
}

// Operador de vizinhan�a (apenas em solu��es completas)
// chamar este operador para actualiza��o de estat�sticas
void COitoDamas::Vizinhanca(TVector<TProcuraMelhorativa*>&vizinhos)
{
	// trocar a posi��o de cada dama
	for(int i=0;i<8;i++)
		for(int j=0;j<8;j++)
			if(j!=damas[i]) {
				COitoDamas *vizinho=(COitoDamas*)Duplicar();
				vizinho->damas[i]=j;
				vizinhos.Add(vizinho);
			}
	TProcuraMelhorativa::Vizinhanca(vizinhos);
}

// muta o estado actual (de acordo com a parametriza��o global)
void COitoDamas::Mutar(void) 
{
	// trocar a posi��o de uma das damas (50%)
	damas[TRand::rand()%8]=TRand::rand()%8;
}

// cruza os dois elementos a e b, colocando o resultado neste
void COitoDamas::Cruzamento(TProcuraMelhorativa *a, TProcuraMelhorativa *b) 
{
	int divisao=1+TRand::rand()%6; // divis�o tem de ter elementos de um e de outro
	damas.Count(8);
	for(int i=0;i<divisao;i++)
		damas[i]=((COitoDamas*)a)->damas[i];
	for(int i=divisao;i<8;i++)
		damas[i]=((COitoDamas*)b)->damas[i];
	TProcuraMelhorativa::Cruzamento(a,b);
}

// retorna a dist�ncia deste elemento ao elemento a 
// (necess�rio para manter os elementos diversos)
long COitoDamas::Distancia(TProcuraMelhorativa*a)
{
	long distancia=0;
	for(int i=0;i<8;i++)
		if(damas[i]!=((COitoDamas*)a)->damas[i])
			distancia++;
	return distancia;
}

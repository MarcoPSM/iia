#pragma once
#include "TProcuraConstrutiva.h"


///////////////////////////////////////////////////////////////////////////////
//	TProcuraMelhorativa class
///////////////////////////////////////////////////////////////////////////////
//	Author: Jos� Coelho
//	Last revision: 2007-09-03
//	Description: 
//    Superclasse de procuras no espa�o das solu��es completas (a solu��o � melhorada).
//    Esta classe n�o tem vari�veis, deve criar as vari�veis necess�rias 
//    � implementa��o dos m�todos virtuais para o problema em concreto.
//    As vari�veis da classe devem ser apenas de uma solu��o, se existir informa��o
//    sobre a inst�ncia, esta informa��o deve ser colocada em vari�veis est�ticas
//    para n�o serem duplicadas desnecessariamente.
//	  A �nica vari�vel � o valor da solu��o que ap�s ser calculada em Avaliar 
//	  deve ser actualizada (se o valor n�o estiver actualizado, deve ter -1)
///////////////////////////////////////////////////////////////////////////////
class TProcuraMelhorativa :
	public TProcuraConstrutiva
{
protected:
	// m�todo interno para libertar objectos n�o necess�rios
	void LibertarVector(TVector<TProcuraMelhorativa*>&vector, int excepto=-1);

public:
	long custo;
public:
	TProcuraMelhorativa(void);
	~TProcuraMelhorativa(void);

	// n�mero de m�ximo de avalia��es (omiss�o 10000)
	static long maximoAvaliacoes; 


	///////////////////////////////////////////////////////////////////////////////
	// M�todos para redefinir
	///////////////////////////////////////////////////////////////////////////////

	void Copiar(TProcuraConstrutiva*objecto) {  
		custo=((TProcuraMelhorativa*)objecto)->custo; 
	}
	// � gerada uma nova solu��o aleatoriamente. 
	// Este m�todo � definido iniciamente como uma procura em profundidade em que o sucessor 
	// � escolhido aleatoriamente e sem retrocesso (caso falhe o processo re-inicia)
	virtual void NovaSolucao(void);
	// Retorna o valor da solu��o completa actual. 
	virtual long Avaliar(void);
	// M�todo que testa se � altura de parar ou n�o (assume-se que o custo 0 � o menor poss�vel)
	virtual bool Parar(void) { return maximoAvaliacoes<=avaliacoes || custo==0; }

	///////////////////////////////////////////////////////////////////////////////
	// Redefinir para poder executar Escalada-do-Monte
	///////////////////////////////////////////////////////////////////////////////

	// Operador de vizinhan�a (apenas em solu��es completas)
	// chamar este operador para actualiza��o de estat�sticas
	virtual void Vizinhanca(TVector<TProcuraMelhorativa*>&vizinhos);

	///////////////////////////////////////////////////////////////////////////////
	// Redefinir para poder executar Algoritmos Gen�ticos
	///////////////////////////////////////////////////////////////////////////////

	// muta o estado actual (de acordo com a parametriza��o global)
	virtual void Mutar(void) { }
	// cruza os dois elementos a e b, colocando o resultado neste
	virtual void Cruzamento(TProcuraMelhorativa*a,TProcuraMelhorativa*b) { geracoes++; }
	// retorna a dist�ncia deste elemento ao elemento a 
	// (necess�rio para manter os elementos diversos)
	virtual long Distancia(TProcuraMelhorativa*a) { return 0; }

	///////////////////////////////////////////////////////////////////////////////
	// Algoritmo de procura local: Escalada do Monte
	///////////////////////////////////////////////////////////////////////////////

	// retorna a avalia��o do resultado actual
	long EscaladaDoMonte(bool movePrimeiro=true);

	///////////////////////////////////////////////////////////////////////////////
	// Algoritmo de procura local: Algoritmos Gen�ticos
	///////////////////////////////////////////////////////////////////////////////

	// retorna a avalia��o do resultado final
	// distanciaMinima � o valor m�nimo que dois elementos podem estar distanciados numa gera��o (Distancia)
	long AlgoritmoGenetico(long populacao=20, long probablidadeMutacao=50, long distanciaMinima=0);


	// M�todo para teste manual do objecto (chamadas aos algoritmos, constru��o de uma solu��o manual)
	// Este m�todo destina-se a testes preliminares, e deve ser redefinido apenas se forem definidos novos algoritmos 
	void TesteManual(const char *nome, long seed=1);

	// Chamar sempre que uma solu��o melhor que a actual � encontrada
	void DebugMelhorEncontrado(void);
};
